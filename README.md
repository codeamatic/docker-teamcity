TeamCity Build Agent
================

 Repository used for storing the necessary files for building TeamCity Build Agent [Docker](http://www.docker.com) images.
 
 ## Agents with JDK7/JDK8
 
* [TeamCity Bare](https://hub.docker.com/r/codeamatic/teamcity-agent-bare/) - Lightweight image with a TeamCity Build Agent, used primarily as a starting point for building heavier images that include software/packages (gradle, maven, bower, grunt, etc) used to actually build a project.
* [TeamCity Enhanced](https://hub.docker.com/r/codeamatic/teamcity-agent-enhanced/) - Heavy image with a TeamCity Build Agent and various software/packages to complement the build process.  Pulls in the TeamCity Bare image.

### Installation

When you start the image as a container (or through docker-compose), you can adjust the configuration by passing one or more of the following variables.

#### TEAMCITY_SERVER (required)
The url of the TeamCity server. 

*TEAMCITY_SERVER=http://example.com:8111*

#### AGENT_NAME
The name/identifier of the agent that shows up within the TeamCity Server UI.

*AGENT_NAME=tcdocker*

```
Command Line:

docker run -e TEAMCITY_SERVER=http://example.com:8111 -e AGENT_NAME=tcdocker -d -p 9090:9090 codeamatic/teamcity-agent-bare

Docker-compose

teamcity-bare:
  image: codeamatic/teamcity-agent-bare
  ports:
    - "9090:9090"
  environment:
    - TEAMCITY_SERVER=http://example.com:8111
    - AGENT_NAME=tcdocker
```